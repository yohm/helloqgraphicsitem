#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_pScene = new QGraphicsScene(QRect(0,0,640,480));
    m_pScene->addRect(0,0,640,480,QPen(Qt::black), QBrush(Qt::white));
    m_pScene->addText("Hello");

    m_pView = new QGraphicsView(m_pScene);
    m_pView->setBackgroundBrush(QBrush(Qt::gray));
    setCentralWidget(m_pView);

    QGraphicsRectItem* pRect = new QGraphicsRectItem(QRectF(0,0,120,130));
    pRect->setBrush(QBrush(Qt::red));
    pRect->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    m_pScene->addItem(pRect);
    qreal theta = 30.0;
    qDebug() << pRect->boundingRect().center();

    pRect->setTransform( QTransform().translate(300,200).rotate(theta) );
}

MainWindow::~MainWindow()
{
    delete ui;
}
